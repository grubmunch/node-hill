let PacketBuilder = require('../../util/packetBuilder')

let formatHex = require('../../util/formatHex')

function bottomPrintAll(message = '', delay = 1) {
    message = formatHex(message)

    let packet = new PacketBuilder()

    packet.buffer.writeUInt8(7)
    
    packet.buffer.writeStringNT('bottomPrint')

    packet.buffer.writeStringNT(message)

    packet.buffer.writeUInt32LE(delay)

    packet.broadCast()
}

module.exports = bottomPrintAll