let PacketBuilder = require('../../util/packetBuilder')

let formatHex = require('../../util/formatHex')

function centerPrintAll(message = '', delay = 1) {
    message = formatHex(message)

    let packet = new PacketBuilder()

    packet.buffer.writeUInt8(7)
    
    packet.buffer.writeStringNT('centerPrint')

    packet.buffer.writeStringNT(message)

    packet.buffer.writeUInt32LE(delay)

    packet.broadCast()
}

module.exports = centerPrintAll