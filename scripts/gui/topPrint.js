let PacketBuilder = require('../../util/packetBuilder')

let formatHex = require('../../util/formatHex')

function topPrint(p, message = '', delay = 1) {

    message = formatHex(message)

    let packet = new PacketBuilder()

    packet.buffer.writeUInt8(7)
    
    packet.buffer.writeStringNT('topPrint')

    packet.buffer.writeStringNT(message)

    packet.buffer.writeUInt32LE(delay)

    packet.send(p.socket)
}

module.exports = topPrint