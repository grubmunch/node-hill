let phin = require('phin')

let { colorTo3D } = require('../../util/color')

const API = 'https://www.brick-hill.com/API/client/getAvatar?id='

function hexToDec(input) {
    return parseInt('0x' + input, 16)
}

async function setAvatar(p, userId) {
    let res = await phin(`${API}${userId || p.userId}`)
    res = res.body.toString().split(',')

    p.partColorHead        = colorTo3D(hexToDec(res[0]))
    p.partColorTorso       = colorTo3D(hexToDec(res[1]))
    p.partColorLArm        = colorTo3D(hexToDec(res[2]))
    p.partColorRArm        = colorTo3D(hexToDec(res[3]))
    p.partColorLLeg        = colorTo3D(hexToDec(res[4]))
    p.partColorRLeg        = colorTo3D(hexToDec(res[5]))
    
    p.partStickerFace      = res[6]
    p.partStickerTShirt    = res[7]
    p.partStickerShirt     = res[8]
    p.partStickerPants     = res[9]
    p.partModelHat1        = res[10]
    p.partModelHat2        = res[11]
    p.partModelHat3        = res[12]
}

module.exports = setAvatar