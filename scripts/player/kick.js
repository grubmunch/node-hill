let PacketBuilder = require('../../util/packetBuilder')

function kick(socket, message = 'Unspecified message.') {
    let packet = new PacketBuilder()

    packet.buffer.writeUInt8(7)
    packet.buffer.writeStringNT('kick')

    packet.buffer.writeStringNT('[You were kicked]\n\nReason: ' + message)

    packet.send(socket)
    
    socket.end()
}

module.exports = kick