let PacketBuilder = require('../../util/packetBuilder')

function sendBrickCount(socket, brickCount = 0) {
    let packet = new PacketBuilder()

    packet.buffer.writeUInt8(1)
    packet.buffer.writeUInt32LE(socket.netId)
    packet.buffer.writeUInt32LE(brickCount)

    packet.send(socket)
}

module.exports = sendBrickCount