module.exports = {
    // Chat
    'messageAll': require('./chat/messageAll'),

    'messageClient': require('./chat/messageClient'),

    // Gui

    'topPrint': require('./gui/topPrint'),

    'topPrintAll': require('./gui/topPrintAll'),

    'centerPrint': require('./gui/centerPrint'),

    'centerPrintAll': require('./gui/centerPrintAll'),

    'bottomPrint': require('./gui/bottomPrint'),

    'bottomPrintAll': require('./gui/bottomPrintAll'),

}