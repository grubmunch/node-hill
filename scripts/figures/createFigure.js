let PacketBuilder = require('../../util/packetBuilder')

function createFigure(socket, player) {
    let packet = new PacketBuilder()

    packet.buffer.writeUInt8(4)
    packet.buffer.writeUInt32LE(player.netId)
    packet.buffer.writeStringNT('ABCDEFGHIKLMNOPQRSTUVWXY')

    packet.buffer.writeFloatLE(player.xPos)
    packet.buffer.writeFloatLE(player.yPos)
    packet.buffer.writeFloatLE(player.zPos)

    packet.buffer.writeUInt32LE(player.xRot)
    packet.buffer.writeUInt32LE(player.yRot)
    packet.buffer.writeUInt32LE(player.zRot)

    packet.buffer.writeFloatLE(player.xScale)
    packet.buffer.writeFloatLE(player.yScale)
    packet.buffer.writeFloatLE(player.zScale)

    //packet.buffer.writeUInt32LE(player.arm)
    packet.buffer.writeUInt32LE(player.partColorHead)
    packet.buffer.writeUInt32LE(player.partColorTorso)
    packet.buffer.writeUInt32LE(player.partColorLArm)
    packet.buffer.writeUInt32LE(player.partColorRArm)
    packet.buffer.writeUInt32LE(player.partColorLLeg)
    packet.buffer.writeUInt32LE(player.partColorRLeg)

    packet.buffer.writeStringNT(player.partStickerFace)
    packet.buffer.writeStringNT(player.partStickerTShirt)
    packet.buffer.writeStringNT(player.partStickerShirt)
    packet.buffer.writeStringNT(player.partStickerPants)

    packet.buffer.writeStringNT(player.partModelHat1)
    packet.buffer.writeStringNT(player.partModelHat2)
    packet.buffer.writeStringNT(player.partModelHat3)

    packet.buffer.writeUInt32LE(player.score)
    packet.buffer.writeUInt32LE(player.team)

    if (!socket)
        packet.broadCastExcept(player.socket)
    else
        packet.send(socket)
}

module.exports = createFigure