let PacketBuilder = require('../../util/packetBuilder')
let formatHex = require('./formatHex')
let messageClient = require('./messageClient')

let rateLimit = new Set()

async function messageAll(message, p = null) {

    if (p && p.muted) {
        return messageClient(p, 'You are muted.')
    }

    if (message.length > 100)
        return Promise.reject(false)

    message = formatHex(message)

    if (p && rateLimit.has(p.username)) {
        messageClient(p, "You're chatting too fast!")
        return Promise.reject(false)
    }

    if (p) {
        rateLimit.add(p.username)
        setTimeout(() => rateLimit.delete(p.username), 2000)
    }

    let packet = new PacketBuilder()

    packet.buffer.writeUInt8(6)
    packet.buffer.writeStringNT(message)

    packet.broadCast()

}

module.exports = messageAll