const PacketBuilder = require('../../util/packetBuilder')

const formatHex = require('../../util/formatHex')

function messageClient(p, message) {
    message = formatHex(message)

    const packet = new PacketBuilder()

    packet.buffer.writeUInt8(6)
    packet.buffer.writeStringNT(message)

    packet.send(p.socket || p)
}

module.exports = messageClient