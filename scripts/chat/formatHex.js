/*

Converts \hhex to <color:HEX> (so the client can interpret it)

Returns the *modified* message

*/

let { rgbToBgr } = require('../../util/color')

const COLOR_REGEX = /(\\h[0-9a-fA-F]{6})/g

function formatHex(input) {
    if (!COLOR_REGEX.test(input)) return input
    
    input.match(COLOR_REGEX).forEach((colorCode) => {
        let hex = colorCode.split('\\h')[1].toUpperCase()
        hex = rgbToBgr(hex)
        input = input.replace(colorCode, `<color:${hex}>`)
    })

    return input
}

module.exports = formatHex