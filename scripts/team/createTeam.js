let PacketBuilder = require('../../util/packetBuilder')

let { hexToDec } = require('../../util/color')

function createTeam(player, team) {
    let packet = new PacketBuilder()

    packet.buffer.writeUInt8(10)

    packet.buffer.writeUInt32LE(team.teamId)

    packet.buffer.writeStringNT(team.name)

    packet.buffer.writeUInt32LE(hexToDec(team.color))

    if (player) {
        packet.send(player.socket)
    } else {
        packet.broadCast()
    }
}

module.exports = createTeam