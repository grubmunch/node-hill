let PacketBuilder = require('../../util/packetBuilder')

// Used for converting camelCase to Brick Hill's weird way of doing it.
const environmentNames = {
    'ambient': 'Ambient',
    'skyColor': 'Sky',
    'baseColor': 'BaseCol',
    'baseSize': 'BaseSize',
    'sunIntensity': 'Sun'
}

const weather = {
    'snow': 'WeatherSnow',
    'rain': 'WeatherRain',
    'sun': 'WeatherSun'
}

function setEnvironment(socket, environment = {}) {
    ['ambient', 'skyColor', 'baseColor', 'baseSize', 'sunIntensity', 'weather'].forEach((prop) => {
        let packet = new PacketBuilder()

        packet.buffer.writeUInt8(7)

        if (prop === 'weather') {
            packet.buffer.writeStringNT(weather[environment.weather])
        } else {
            packet.buffer.writeStringNT(environmentNames[prop])
            packet.buffer.writeUInt32LE(environment[prop])
        }

        packet.send(socket)
    })
}

module.exports = setEnvironment