let Game = require('../../class/game')

let { addBrickProperties } = require('./sendBrick')

let PacketBuilder = require('../../util/packetBuilder')

function loadMap(socket) {
    let packet = new PacketBuilder(Game.mapBuffer)

    if (Game.mapBuffer)
        return packet.send(socket)

    if (!Game.world.bricks.length) return

    packet.buffer.writeUInt8(2)

    for (let brick of Game.world.bricks)
        addBrickProperties(packet, brick)

    return packet.buffer
}

module.exports = loadMap