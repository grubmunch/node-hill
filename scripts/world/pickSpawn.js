/*

    Chooses a random spawn based on either the size of the baseplate, or if any spawnbricks are present
    it will choose a random spawn point.

*/

let Game = require('../../class/game')

function generateRandomInteger(min, max) {
    return Math.floor(min + Math.random() * (max + 1 - min))
}

function pickSpawn() {
    const SPAWN_LENGTH = Game.world.spawns.length

    if (SPAWN_LENGTH > 0) {
        const SPAWN_BRICK = Game.world.spawns[Math.floor(Math.random() * SPAWN_LENGTH)]
        return [ // X, Y, Z
            SPAWN_BRICK.xPos + SPAWN_BRICK.xScale/2,
            SPAWN_BRICK.yPos + SPAWN_BRICK.yScale/2,
            SPAWN_BRICK.zPos + SPAWN_BRICK.zScale/2
        ]
    }

    const BASE_SIZE = Game.world.environment.baseSize

    return [ // X, Y, Z
        generateRandomInteger(-BASE_SIZE/2, BASE_SIZE/2),
        generateRandomInteger(-BASE_SIZE/2, BASE_SIZE/2),
        BASE_SIZE/2
    ]

}

module.exports = pickSpawn