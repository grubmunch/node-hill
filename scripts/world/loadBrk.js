let fs = require('fs')

// Utility + tools
let { generateColor, generateSkyColor, rgbToHex } = require('../../util/color')

let { Game, Team, Brick } = require('../../class')

let loadMap = require('./loadMap')

function loadBrk(map) {
    const FILE = fs.readFileSync(map, 'utf-8')

    const LINES = FILE.split('\n')

    let totalLines = 0
    let bricks = []
    let teams = []
    let currentBrick = -1
    
    let scriptWarning = false // Doesn't load scripts, just notifies if the .brk contains scripts

    for (let line of LINES) {

        totalLines++

        line = line.trim()

        // Set up environment details
        switch (totalLines) {
            case 1: {
                if (line !== `B R I C K  W O R K S H O P  V0.2.0.0`) {
                    console.error('ERROR: This set was created using an incompatible version of Brick Hill.')
                    return process.exit(0)
                }
                continue
            }
            case 3: {
                const RGB = line.split(' ')
                Game.world.environment['ambient'] = generateColor(RGB[0], RGB[1], RGB[2])
                continue
            }
            case 4: {
                const RGB = line.split(' ')
                Game.world.environment['baseColor'] = generateColor(RGB[0], RGB[1], RGB[2])
                continue
            }
            case 5: {
                const RGB = line.split(' ')
                Game.world.environment['skyColor'] = generateSkyColor(RGB[0], RGB[1], RGB[2])
                continue
            }
            case 6: {
                Game.world.environment['baseSize'] = Number(line)
                continue
            }
            case 7: {
                Game.world.environment['sunIntensity'] = Number(line)
                continue
            }
        }

        const DATA = line.split(' ')

        const ATTRIBUTE = DATA[0].replace('+', '')

        const VALUE = DATA.slice(1).join(' ')

        switch (ATTRIBUTE) {
            case 'NAME': {
                bricks[currentBrick].name = VALUE
                continue
            }
            case 'ROT': {
                bricks[currentBrick].rotation = VALUE
                continue
            }
            case 'SHAPE': {
                bricks[currentBrick].shape = VALUE
                if (VALUE === "spawnpoint")
                    Game.world.spawns.push(bricks[currentBrick])
                continue
            }
            case 'MODEL': {
                bricks[currentBrick].model = VALUE
                continue
            }
            case 'NOCOLLISION': {
                bricks[currentBrick].collision = false
                continue
            }
            case 'COLOR': {
                const color = VALUE.split(' ')
                let team = new Team(
                    teams[teams.length - 1], 
                    rgbToHex(
                        Math.round(color[0] * 255), 
                        Math.round(color[1] * 255), 
                        Math.round(color[2] * 255)
                    )
                )
                teams[teams.length - 1] = team
                continue
            }
            case 'SCRIPT': {
                if (scriptWarning) continue;
                scriptWarning = true
                console.warn('WARNING: This set contains scripts, scripts are incompatible with node-hill so they will not be loaded.')
            }
        }

        if (DATA.length === 10) {

            const xPos  = DATA[0],

                yPos    = DATA[1],

                zPos    = DATA[2],

                xScale  = DATA[3],

                yScale  = DATA[4],

                zScale  = DATA[5],

                color   = rgbToHex(
                    Math.round(DATA[6] * 255), 
                    Math.round(DATA[7] * 255), 
                    Math.round(DATA[8] * 255)
                ),

                alpha   = DATA[9];
            
            const newBrick = new Brick([xPos, yPos, zPos], [xScale, yScale, zScale], color, alpha)

            newBrick.properties = line

            bricks.push(newBrick)

            currentBrick++
        }

        if (DATA[0] && DATA[0] === '>TEAM')
            teams.push(DATA[1])

    }

    return loadMap() // Create a buffer of the map

}

module.exports = loadBrk