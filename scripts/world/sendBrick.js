let PacketBuilder = require('../../util/packetBuilder')

let { hexToRgb } = require('../../util/color')

function addBrickProperties(packet, brick) {
    if (!brick.properties) {
        brick.properties = `${brick.xPos} ${brick.yPos} ${brick.zPos} `
        brick.properties += `${brick.xScale} ${brick.yScale} ${brick.zScale} `

        const rgb = hexToRgb(brick.color)

        brick.properties += `${Math.round(100 * rgb[0] / 255) / 100} `
        brick.properties += `${Math.round(100 * rgb[1] / 255) / 100} `
        brick.properties += `${Math.round(100 * rgb[2] / 255) / 100} `
        brick.properties += `${brick.alpha}`
    }

    packet.buffer.writeStringNT(brick.properties + '\r\n')

    if (brick.name)
        packet.buffer.writeStringNT(`\t+NAME ${brick.brickId}\r\n`)

    if (brick.shape)
        packet.buffer.writeStringNT(`\t+SHAPE ${brick.shape}\r\n`)

    if (brick.rotation)
        packet.buffer.writeStringNT(`\t+ROT ${brick.rotation}\r\n`)
        
    if (brick.model)
        packet.buffer.writeStringNT(`\t+MODEL ${brick.model}\r\n`)
        
    if (!brick.collision)
        packet.buffer.writeStringNT('\t+NOCOLLISION\r\n')
    
}

function sendBrick(socket, brick) {
    let packet = new PacketBuilder()

    packet.buffer.writeUInt8(2)

    addBrickProperties(packet, brick)

    return packet.send(socket)
}

module.exports = { sendBrick, addBrickProperties }