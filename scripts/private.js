module.exports = {
    // Chat
    'messageAll': require('./chat/messageAll'),

    'messageClient': require('./chat/messageClient'),
    
    // Player
    'kick': require('./player/kick'),

    'sendBrickCount': require('./player/sendBrickCount'),

    'setAvatar': require('./player/setAvatar'),

    'createFigure': require('./figures/createFigure'),

    // Team
    'createTeam': require('./team/createTeam'),

    // World
    'setEnvironment': require('./world/setEnvironment'),

    'loadBrk': require('./world/loadBrk'),

    'loadMap': require('./world/loadMap'),

    'pickSpawn': require('./world/pickSpawn'),
}