/*
    Hey, you found this module. That's cool. See that API down there?

    Yeah, that's not so cool, but if you modify this code to post a 
    
    high player count, your account is going to get terminated.

    So, please keep this between us. Thanks, love you xoxo. 
    
    ~ Jake.

*/

let p = require('phin')

let { Game } = require('../class')

const API = 'https://www.brick-hill.com/API/games/postServer'

async function postServer() {
    try {
        await p({
            url: API,
            method: 'POST',
            form: {
                'GAME': Game.gameId,
                'PORT': Game.port,
                'PLAYERS': Game.playerCount,
                '' : ''
            }
        })
    } catch (err) {
        console.warn('Failure posting to master server.')
    }
    setTimeout(postServer, 1000 * 60)
}

module.exports = postServer
