let p = require('phin')

let Game = require('../class/game')

function sanitize(username) {
    return username.trim().replace(/&/g, '') // Subtle hint at a client bug here :^)
}

async function checkAuth(socket, reader) {
        // Don't use any of this, it needs to be verified.
        const USER = {
            username: sanitize(reader.readStringNT()),
            userId: Number(reader.readUInt32LE()),
            version: reader.readStringNT()
        }
        
        try {
            // Version check
            if (USER.version !== '0.2.0.0')
                return [false, 'Your client is out of date.']

            console.log(`Attempting to authenticate: ${USER.username}.`)

            if (Game.local) {
                return [{
                    username: USER.username,
                    userId: USER.userId,
                    admin: false
                }]
            }

            let res = await p(`https://www.brick-hill.com/API/games/auth2?USERNAME=${USER.username}&IP=${socket.IPV4}`)
            res = res.body.toString().split(' ')
    
            const VERIFIED = res[0]
            const USER_ID = Number(res[1])
            const ADMIN = Number(res[2])

            if (VERIFIED === 'TRUE' && USER_ID === USER.userId) {
                return [{
                    username: USER.username,
                    userId: USER.userId,
                    admin: ADMIN
                }]
            }
            
            return [false, 'Your user could not be verified.\nTry logging out of your account and back in.']

        } catch (err) {
            console.warn(`<Error while authenticating: ${USER.username}>`, err.message)
            return [{}, 'Server error while authenticating.']
        }
}

module.exports = checkAuth