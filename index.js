const { resolve, basename } = require('path')

const { NodeVM }  = require('vm2')

const fs = require('fs')

const Game = require('./class/game')

const loadBrk = require('./scripts/world/loadBrk')

const DEFAULT_VM_SETTINGS = {
    require: { 
        builtin: [], 
        root: './', 
        external: [
            'discord.js', 
            'request', 
            'request-promise',
            'phin',
            'enmap'
        ]
    }
}

function loadScripts() {
    let { Game, Team, Brick } = require('./class')
    
    let scripts = require('./scripts')

    if (!Game.vm) Game.vm = DEFAULT_VM_SETTINGS

    Game.vm.sandbox = {
        Game: Game,
        Team: Team,
        Brick: Brick,
        scripts: scripts
    }

    const vm = new NodeVM(Game.vm)
    
    // Import your scripts and run them in the VM
    fs.readdirSync(Game.userScripts).forEach((file) => {
        if (file.endsWith('.js')) {
            try {
                const script = fs.readFileSync(resolve(Game.userScripts, file), 'UTF-8')
                vm.run(script, 'user_script.js')
            } catch (err) {
                console.warn('Failure loading script: ' + file)
                console.log(err.stack)
            }
        }
    })
}

function initiateMap(map) {

    if (!basename(map).endsWith('.brk')) {
        console.log('Map selected is not a .brk file. Aborting.')
        return process.exit(0)
    }

    console.clear()

    try {
        Game.mapBuffer = loadBrk(map) // Load map (environment, bricks, spawns) into mapBuffer.
    } catch (err) {
        console.error('Failure parsing brk: ', err && err.stack)
        return process.exit(1)
    }

    console.log(`Selected: <Port: ${Game.port} | Game: ${Game.gameId} | Map: ${basename(map)}>`)
}

function startServer(settings) {
    let { port, gameId, map, scripts, vm, local } = settings

    if (!port || isNaN(port)) {
        console.log('No port specified. Defaulted to 42480.')
        port = 42480
    }

    if (!gameId || isNaN(gameId)) {
        console.log('No game ID specified.')
        return process.exit(0)
    }

    if (!map) {
        console.log('No map loaded.')
        return process.exit(0)
    }

    Game.port = port

    Game.gameId = gameId

    Game.vm = vm

    Game.userScripts = resolve(process.cwd(), scripts)

    Game.local = local

    Game.map = resolve(process.cwd(), map)

    initiateMap(Game.map)

    loadScripts()

    require('./server')
}

module.exports = { startServer }