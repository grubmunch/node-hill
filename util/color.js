function makeColorRGB(r, g, b) {
    return (r << 0) + (g << 8) + (b << 16)
}

function colorTo3D(col) {
    const b = (col & 0xff0000) >> 16
    const g = (col & 0xff00) >> 8
    const r = (col & 0xff)
    return Number(makeColorRGB(b, g, r))
}

function rgbToBgr(rgb) {
    rgb = rgb.toString()
    return rgb.substring(4, 6) + rgb.substring(2, 4) + rgb.substring(0, 2)
}

function rgbToHex(r, g, b) {
    const rgb = b | (g << 8) | (r << 16);
    return '#' + (0x1000000 + rgb).toString(16).slice(1)
}

function hexToRgb(h) {
    h = h.replace(/^#/, '')
    return [
        parseInt(h.substring(0, 2), 16),
        parseInt(h.substring(2, 4), 16),
        parseInt(h.substring(4, 6), 16)
    ]
}

function hexToDec(hex) {
    hex = hex.replace(/^#/, '')
    hex = rgbToBgr(hex)
    return parseInt(hex, 16) 
}

function generateColor(r, g, b) {
    const col = makeColorRGB(r * 255, g * 255, b * 255)
    return colorTo3D(col)
}

function generateSkyColor(r, g, b) {
    const col = makeColorRGB(r * 255, g * 255, b * 255)
    return colorTo3D(colorTo3D(col))
}

module.exports = { 
    generateColor,
    hexToRgb,
    rgbToBgr,
    hexToDec,
    rgbToHex,
    generateSkyColor, 
    colorTo3D 
}