let Game = require('../class/game')

let SmartBuffer = require('smart-buffer').SmartBuffer

let zlib = require('zlib')

let uintv = require('./uintv')

class PacketBuilder {
    constructor(buffer, compression = true) {
        this.compression = compression
        this.buffer = buffer || new SmartBuffer() // Set the buffer to an existing one, or start fresh.
    }

    // Convert SmartBuffer to a buffer, compress it, and add uintv size to header.
    transformPacket() {
        let packet = this.buffer.toBuffer()
        if (this.compression)
            packet = zlib.deflateSync(packet)
        return uintv.writeUIntv(packet) // Prefix buffer with the length of the message.
    }

    broadCastExcept(socket) {
        let packet = this.transformPacket()
        for (let player of Game.players) {
            if (player.netId !== socket.netId) {
                player.socket.write(packet)
            }
        }
    }

    // Send a packet to every connected client.
    broadCast() {
        let packet = this.transformPacket()
        for (let player of Game.players)
            player.socket.write(packet)
    }

    // Send a packet to a single client.
    send(socket) {
        let packet = this.transformPacket()
        socket.write(packet)
    }
}

module.exports = PacketBuilder