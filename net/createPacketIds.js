const PacketBuilder = require('../util/packetBuilder')

function createIdBuffer(player, idString = "", broadCast = false) {
    const length = idString.length

    if (!length) return

    const globalPacket = new PacketBuilder()

    globalPacket.buffer.writeUInt8(4)

    globalPacket.buffer.writeUInt32LE(player.netId)

    globalPacket.buffer.writeStringNT(idString)

    for (let i = 0; i < length; i++) {
        const ID = idString.charAt(i)
        switch (ID) {
            case "G": {
                globalPacket.buffer.writeFloatLE(player.xScale)
                break
            }
            case "H": {
                globalPacket.buffer.writeFloatLE(player.yScale)
                break
            }
            case "I": {
                globalPacket.buffer.writeFloatLE(player.zScale)
                break
            }
            case "K": {
                globalPacket.buffer.writeUInt32LE(player.partColorHead)
                break
            }
            case "L": {
                globalPacket.buffer.writeUInt32LE(player.partColorTorso)
                break
            }
            case "M": {
                globalPacket.buffer.writeUInt32LE(player.partColorLArm)
                break
            }
            case "N": {
                globalPacket.buffer.writeUInt32LE(player.partColorRArm)
                break
            }
            case "O": {
                globalPacket.buffer.writeUInt32LE(player.partColorLLeg)
                break
            }
            case "P": {
                globalPacket.buffer.writeUInt32LE(player.partColorRLeg)
                break
            }
            case "Q": {
                globalPacket.buffer.writeStringNT(player.partStickerFace)
                break
            }
            case "R": {
                globalPacket.buffer.writeStringNT(player.partStickerTShirt)
                break
            }
            case "S": {
                globalPacket.buffer.writeStringNT(player.partStickerShirt)
                break
            }
            case "T": {
                globalPacket.buffer.writeStringNT(player.partStickerPants)
                break
            }
            case "U": {
                globalPacket.buffer.writeStringNT(player.partModelHat1)
                break
            }
            case "V": {
                globalPacket.buffer.writeStringNT(player.partModelHat2)
                break
            }
            case "W": {
                globalPacket.buffer.writeStringNT(player.partModelHat3)
                break
            }
            case "X": {
                globalPacket.buffer.writeUInt32LE(player.score)
                break
            }
            case "Y": {
                globalPacket.buffer.writeUInt32LE(player.team)
                break
            }
        }
    }

    if (!broadCast)
        return globalPacket.broadCastExcept(player.socket)
    else
        return globalPacket.broadCast()
}

module.exports = createIdBuffer