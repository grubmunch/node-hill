const PacketBuilder = require('../util/packetBuilder')

function createMovementIdBuffer(player, idString = "") {
    const length = idString.length

    const movementPacket = new PacketBuilder()

    movementPacket.buffer.writeUInt8(4)

    movementPacket.buffer.writeUInt32LE(player.netId)

    movementPacket.buffer.writeStringNT(idString)

    for (let i = 0; i < length; i++) {
        const ID = idString.charAt(i)
        switch (ID) {
            case "A": {
                movementPacket.buffer.writeFloatLE(player.xPos)
                break
            }
            case "B": {
                movementPacket.buffer.writeFloatLE(player.yPos)
                break
            }
            case "C": {
                movementPacket.buffer.writeFloatLE(player.zPos)
                break 
            }
            case "F": {
                movementPacket.buffer.writeUInt32LE(player.zRot)
                break 
            }
        }
    }

    movementPacket.broadCastExcept(player.socket)

}

module.exports = createMovementIdBuffer