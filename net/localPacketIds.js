const PacketBuilder = require('../util/packetBuilder')

function createLocalIdBuffer(player, idString = "") {
    const length = idString.length

    const localPacket = new PacketBuilder()

    localPacket.buffer.writeUInt8(4)

    localPacket.buffer.writeUInt32LE(player.netId)

    localPacket.buffer.writeStringNT(idString)

    for (let i = 0; i < length; i++) {
        const ID = idString.charAt(i)
        switch (ID) {
            case "A": {
                localPacket.buffer.writeFloatLE(player.xPos)
                break
            }
            case "B": {
                localPacket.buffer.writeFloatLE(player.yPos)
                break
            }
            case "C": {
                localPacket.buffer.writeFloatLE(player.zPos)
                break 
            }
            case "D": {
                localPacket.buffer.writeUInt32LE(player.xRot)
                break
            }
            case "E": {
                localPacket.buffer.writeUInt32LE(player.yRot)
                break
            }
            case "F": {
                localPacket.buffer.writeUInt32LE(player.zRot)
                break 
            }
            case "M": {
                localPacket.buffer.writeFloatLE(player.health)
                break
            }
            case "N": {
                localPacket.buffer.writeUInt32LE(player.partColorHead)
                break
            }
            case "O": {
                localPacket.buffer.writeUInt32LE(player.partColorTorso)
                break
            }
            case "P": {
                localPacket.buffer.writeUInt32LE(player.partColorLArm)
                break
            }
            case "Q": {
                localPacket.buffer.writeUInt32LE(player.partColorRArm)
                break
            }
            case "R": {
                localPacket.buffer.writeUInt32LE(player.partColorLLeg)
                break
            }
            case "S": {
                localPacket.buffer.writeUInt32LE(player.partColorRLeg)
                break
            }
            case "T": {
                localPacket.buffer.writeStringNT(player.partStickerFace)
                break
            }
            case "U": {
                localPacket.buffer.writeStringNT(player.partStickerTShirt)
                break
            }
            case "V": {
                localPacket.buffer.writeStringNT(player.partStickerShirt)
                break
            }
            case "W": {
                localPacket.buffer.writeStringNT(player.partStickerPants)
                break
            }
            case "X": {
                localPacket.buffer.writeStringNT(player.partModelHat1)
                break
            }
            case "Y": {
                localPacket.buffer.writeStringNT(player.partModelHat2)
                break
            }
            case "Z": {
                localPacket.buffer.writeStringNT(player.partModelHat3)
                break
            }
            case "0": {
                localPacket.buffer.writeUInt32LE(player.score)
                break
            }
            case "1": {
                localPacket.buffer.writeUInt32LE(player.maxSpeed)
                break
            }
            case "2": {
                localPacket.buffer.writeUInt32LE(player.maxJumpHeight)
                break
            }
            case "3": {
                localPacket.buffer.writeUInt32LE(player.FOV)
                break
            }
            case "4": {
                localPacket.buffer.writeUInt32LE(player.camDist)
                break
            }
            case "5": {
                localPacket.buffer.writeUInt32LE(player.camXPos)
                break
            }
            case "6": {
                localPacket.buffer.writeUInt32LE(player.camYPos)
                break
            }
            case "7": {
                localPacket.buffer.writeUInt32LE(player.camZPos)
                break
            }
            case "8": {
                localPacket.buffer.writeUInt32LE(player.camXRot)
                break
            }
            case "9": {
                localPacket.buffer.writeUInt32LE(player.camYRot)
                break
            }
            case "a": {
                localPacket.buffer.writeUInt32LE(player.camZRot)
                break
            }
            case "b": {
                switch (player.camType) {
                    case "fixed": {
                        localPacket.buffer.writeUInt32LE(0)
                        break
                    }
                    case "orbit": {
                        localPacket.buffer.writeUInt32LE(1)
                        break
                    }
                    case "free": {
                        localPacket.buffer.writeUInt32LE(2)
                        break
                    }
                    case "first": {
                        localPacket.buffer.writeUInt32LE(3)
                        break
                    }
                }
                break
            }
            case "c": {
                localPacket.buffer.writeUInt32LE(player.camObj)
                break
            }
            case "d": {
                localPacket.buffer.writeUInt32LE(player.team)
                break
            }
        }
    }

    localPacket.send(player.socket)

}

module.exports = createLocalIdBuffer