// Dependencies
let SmartBuffer = require('smart-buffer').SmartBuffer
let zlib = require('zlib')

// Import utilities
let uintv = require('../util/uintv')

// Game objects

let { Game, Player } = require('../class')

let scripts = require('../scripts/private')

let checkAuth = require('../api/checkAuth')

async function packetHandler(socket, packet) {
    const IP = socket.IP

    // Remove size prefix from the message.
    const BYTE_SIZE = uintv.readUIntv(packet)
    packet = packet.slice(BYTE_SIZE.end)

    try {
        packet = zlib.inflateSync(packet)
    } catch (err) {
        // Packet isn't always compressed.   
    }

    // Create a smart buffer.
    const READER = SmartBuffer.fromBuffer(packet)

    // Get type of packet.
    const TYPE = READER.readUInt8()

    let player = socket.player

    if (TYPE === 1 && player)  return // You can't authenticate more than once.
    if (TYPE !== 1 && !player) return // You don't have a player.

    switch (TYPE) {
        case 1: { // Authentication
            const [ USER, ERR_MSG ] = await checkAuth(socket, READER)

            // User could not authenticate properly.
            if (Object.keys(USER).length === 0) {
                console.log(`<Client: ${IP}> Failed verification.`)
                return scripts.kick(socket, ERR_MSG || 'Server error.')
            }

            console.log(`Successfully verified! (Username: ${USER.username} | ID: ${USER.userId} | Admin: ${Boolean(USER.admin)})`)

            socket.netId = Game.netId++

            scripts.sendBrickCount(socket, Game.world.bricks.length)

            let newPlayer = new Player(socket)

            newPlayer.userId = USER.userId
            newPlayer.username = USER.username
            newPlayer.admin = USER.admin

            Game._newPlayer(newPlayer)

            break
        }
        case 2: { // Update player position
            try {
                player._updatePositionForOthers([
                    READER.readFloatLE(),  // xpos
                    READER.readFloatLE(),  // ypos
                    READER.readFloatLE(),  // zpos
                    READER.readUInt32LE()  // zrot
                ], true)

                READER.readFloatLE()
                READER.readFloatLE()
                READER.readFloatLE()
                READER.readFloatLE()

                const onClick = READER.readUInt8()
                
                if (onClick) {
                    player.emit('clicked')
                }
            } catch (err) {
                // You get read errors often, It's safe to just ignore them.
            }
            break
        }
        case 3: { // Commands
            const COMMAND   = READER.readStringNT() + 'Cmd',
                ARGS        = READER.readStringNT();

            if (COMMAND === 'resetCmd') {
                if (Game.resetEnabled) {
                    player.respawn()
                }
            }

            if (COMMAND !== 'chatCmd')
                return Game.emit(COMMAND, player, ARGS)

            scripts.messageAll(`\\c8${player.username}\\c1:\\c0 ` + ARGS, player)
                .then(()    => console.log(`${player.username}: ${ARGS}`))
                .catch(()   => {})
            
            Game.emit('chatted', player, ARGS)

            break
        }
        case 4: { // Projectiles
            break
        }
    }
}

module.exports = { packetHandler }