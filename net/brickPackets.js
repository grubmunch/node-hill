let PacketBuilder = require('../util/packetBuilder')

let { colorTo3D } = require('../util/color')

function hexToDec(input) {
    return parseInt('0x' + input, 16)
}

function createBrickPacket(brick, modification) {
    let brickPacket = new PacketBuilder(null, false)

    brickPacket.buffer.writeUInt8(9)

    brickPacket.buffer.writeUInt32LE(brick.brickId)

    brickPacket.buffer.writeStringNT(modification)

    switch (modification) {
        case 'pos': {
            brickPacket.buffer.writeFloatLE(brick.xPos)
            brickPacket.buffer.writeFloatLE(brick.yPos)
            brickPacket.buffer.writeFloatLE(brick.zPos)
            break
        }
        case 'rot': {
            brickPacket.buffer.writeUInt32LE(brick.rotation)
            break
        }
        case 'scale': {
            brickPacket.buffer.writeFloatLE(brick.xScale)
            brickPacket.buffer.writeFloatLE(brick.yScale)
            brickPacket.buffer.writeFloatLE(brick.zScale)
            break
        }
        case 'kill': {
            break
        }
        case 'destroy': {
            break
        }
        case 'col': {
            colorTo3D(hexToDec(brickPacket.buffer.writeUInt32LE(brick.color)))
            break
        }
        case 'model': {
            brickPacket.buffer.writeStringNT(brick.model)
            break
        }
        case 'collide': {
            brickPacket.buffer.writeUInt8(brick.collision ? 1: 0)
            break
        }
    }
        
    brickPacket.broadCast()

}

module.exports = createBrickPacket