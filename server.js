// Import dependencies
let net = require('net')

let p = require('phin').defaults({parse: 'json'})

// Import game data
let { Game } = require('./class')

// Post server
let postServer = require('./api/postServer')

// Connection + packet handler
let { packetHandler } = require('./net/packetHandler')

// Create socket server.
const SERVER = net.createServer(socketConnection)

function maskIP(ip) {
    ip = ip.split('.').splice(0, 2)
    return ip.join('.') + '.x.x'
}

async function getHostIP() {
    let req = await p('https://api.ipify.org/?format=json')
    return req.body.ip
}

async function socketConnection(client) {
    client.IPV4 = client.remoteAddress

    if (client.IPV4.startsWith('192.168.'))
        client.IPV4 = await getHostIP()

    client.IP = maskIP(client.IPV4)

    console.log(`<New client: ${client.IP}>`)

    client.setNoDelay(true)

    client.setKeepAlive(true, 10000)

    client.on('data', (PACKET) => packetHandler(client, PACKET)
        .catch(console.error)
    )

    client.on('close', () => {
        console.log(`<Client: ${client.IP}> Lost connection.`)
        Game._playerLeft(client.player)
    })

    client.on('error', () => client.destroy())
}

let SERVER_LISTEN_ADDRESS = '0.0.0.0'

if (Game.local) {
    SERVER_LISTEN_ADDRESS = '127.0.0.1'
    Game.port = 42480
}

SERVER.listen(Game.port, SERVER_LISTEN_ADDRESS, () => {
    console.log('Listening on port ' + Game.port)
    if (!Game.local)
        postServer()
    else
        console.log('Running server locally.')
})