let Game = require('./game')

let { createTeam } = require('../scripts/private')

class Team {
    constructor(name, color) {
        this.name = name

        this.color = color

        this.teamId = Game.teams.length

        Game.teams.push(this)

        this._create()
    }

    _create() {
        createTeam(null, this)
    }
    
}

module.exports = Team