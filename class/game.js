let EventEmitter = require('events')

class Game extends EventEmitter {
    constructor() {
        super()

        this.players = new Set()

        this.playerCount = 0

        this.netId = 0

        this.teams = []

        this.resetEnabled = true

        this.assignRandomTeam = true

        this.playersSpawn = true

        this.systemMessages = true

        this.world = {
            environment: {
                ambient: 0,
                skyColor: 0xe6b271,
                baseColor: 0x248233,
                baseSize: 100,
                sunIntensity: 400,
                weather: 'sun'
            },
            bricks: [],
            spawns: []
        }
    }

    setEnvironment(environment = {}) {
        const { setEnvironment } = require('../scripts/private')

        for (let change of Object.keys(environment))
            this.world.environment[change] = environment[change]
        
        for (let player of this.players)
            setEnvironment(player.socket, environment)
    }

    findTeamByName(name) {
        for (let team of this.teams) {
            if (team.name === name)
                return team
        }
    }

    findPlayerByUsername(username) {
        for (let player of this.players) {
            if (player.username === username)
                return player
        }
    }

    findPlayerByUserId(userId) {
        for (let player of this.players) {
            if (player.userId === userId)
                return player
        }
    }

    findBrickByName(name) {
        for (let brick of this.world.bricks) {
            if (brick.name === name)
                return brick
        }
    }

    _newPlayer(p) {
        for (let player of this.players) {
            if (player.username === p.username)
                return p.kick('You can only join once per account.')
        }
        if (p.socket.destroyed) return // Precaution
        p.authenticated = true
        p.socket.player = p
        this.players.add(p)
        this.playerCount++ // Increase global playercount.
        p._joined()
        this.emit('playerJoin', p)
    }

    _playerLeft(p) {
        if (p && p.authenticated) {
            this.players.delete(p)
            this.playerCount--
            p._left()
            this.emit('playerLeave', p)
        }
    }
}

module.exports = new Game()