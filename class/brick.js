let Game = require('./game')

let EventEmitter = require('events')

let createBrickPacket = require('../net/brickPackets')

class Brick extends EventEmitter {
    constructor(pos, scale, color, alpha = 1) {
        super()

        this.xPos = Number(pos[0])

        this.yPos = Number(pos[1])

        this.zPos = Number(pos[2])

        this.position = pos

        this.xScale = Number(scale[0])

        this.yScale = Number(scale[1])

        this.zScale = Number(scale[2])

        this.scale = scale

        this.color = color

        this.alpha = Number(alpha)

        this.brickId = Game.world.bricks.length

        this.collision = true

        this.on('newListener', (event) => {
            if (!event === 'touching') return
            this._detectTouching()
        })

        this.on('removeListener', (event) => {
            if (!event === 'touching') return
            if (this.listenerCount('touching')) return
            clearInterval(this.touchDetection)
        })

        Game.world.bricks.push(this)

    }

    setPosition(pos) {
        this.xPos = pos[0]
        this.yPos = pos[1]
        this.zPos = pos[2]
        createBrickPacket(this, 'pos')
    }

    setScale(scale) {
        this.xScale = scale[0]
        this.yScale = scale[1]
        this.zScale = scale[2]
        createBrickPacket(this, 'scale')
    }

    setRotation(rot) {
        if (rot % 90 !== 0) {
            throw new Error('Rotation must be dividable by 90.')
        }
        this.rotation = rot
        createBrickPacket(this, 'rot')
    }

    destroy() {
        this.destroyed = true
        delete Game.world.bricks[this]
        createBrickPacket(this, 'destroy')
    }

    setModel(model) {
        this.model = model
        createBrickPacket(this, 'model')
    }

    setColor(color) {
        this.color = color
        createBrickPacket(this, 'col')
    }

    // I would love to make this toggleable, but there's a client bug :^(
    disableCollision() {
        this.collision = false
        createBrickPacket(this, 'collide')
    }

    _hitDetection() {
        let scale   = []
        let origin  = []

        scale[0]    = this.xScale/2
        scale[1]    = this.yScale/2
        scale[2]    = this.zScale/2
        origin[0]   = this.xPos + scale[0]
        origin[1]   = this.yPos + scale[1]
        origin[2]   = this.zPos + scale[2]    

        for (let p of Game.players) {

            let size    = []
            let center  = []

            size[0]     = p.xScale
            size[1]     = p.yScale
            size[2]     = 5 * p.zScale/2
            center[0]   = p.xPos
            center[1]   = p.yPos
            center[2]   = p.zPos + size[2]

            let touched = true

            for (let i = 0; i < 3; i++) {
                let dist = Math.abs(origin[i] - center[i])
                let close = size[i] + scale[i]
                if (dist >= close + 0.4) {
                    touched = false
                }
            }

            if (touched) {
                return p
            }
        }
    }

    touched(callBack) {
        this.once('touching', callBack)
    }

    touching(callBack) {
        this.on('touching', callBack)
    }

    disableTouching(callBack) {
        this.off('touching', callBack)
    }

    _detectTouching() {
        this.touchDetection = setInterval(() => {
            if (this.destroyed) {
                clearInterval(this.touchDetection)
            }
            const p = this._hitDetection()
            if (p) {
                this.emit('touching', p)
            }
        }, 50)
    }
}

module.exports = Brick