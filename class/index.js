module.exports = {
    Game: require('./game'),

    Brick: require('./brick'),

    Player: require('./player'),

    Team: require('./team')
}