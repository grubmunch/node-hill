let EventEmitter = require('events')

let Game = require('./game')

let scripts = require('../scripts/private')

let PacketBuilder = require('../util/packetBuilder')

let createPacketIds = require('../net/createPacketIds')

let createLocalIdBuffer = require('../net/localPacketIds')

let createMovementIdBuffer = require('../net/movementPackets')

class Player extends EventEmitter {
    constructor(socket) {
        super()

        this.socket = socket
        this.netId = socket.netId

        // Positioning
        this.xPos = 0
        this.yPos = 0
        this.zPos = 0
        this.xRot = 0
        this.yRot = 0
        this.zRot = 0

        // Size
        this.xScale = 1
        this.yScale = 1
        this.zScale = 1

        // Camera properties
        this.FOV = 60
        this.camDist = 5
        this.camXPos = 0
        this.camYPos = 0
        this.camZPos = 50
        this.camXRot = 0
        this.camYRot = 0
        this.camZRot = 0
        this.camType = "fixed"
        this.camObj = null

        // Assets + color
        this.arm = 0
        this.partColorHead = 0
        this.partColorTorso = 0
        this.partColorLArm = 0
        this.partColorRArm = 0
        this.partColorLLeg = 0
        this.partColorRLeg = 0
        this.partStickerFace = "0"
        this.partStickerTShirt = "0"
        this.partStickerShirt = "0"
        this.partStickerPants = "0"
        this.partModelHat1 = "0"
        this.partModelHat2 = "0"
        this.partModelHat3 = "0"

        // Stats
        this.maxHealth = 100
        this.health = this.maxHealth
        this.alive = false
        this.muted = false
        this.maxSpeed = 4
        this.maxJumpHeight = 5
        this.toolNum = 0
        this.score = 0
        this.team = 0
    }

    kick(message = null) {
        return scripts.kick(this.socket, message)
    }

    _log(message, broadCast = false) {
        if (!Game.systemMessages) return
        
        if (broadCast)
            scripts.messageAll(message)
        else
            scripts.messageClient(this.socket, message)
    }

    _left() {
        console.log(`${this.username} has left the game.`)
        this._removeFromPlayerList()
        this._log(`\\c6[SERVER]: \\c0${this.username} has left the server!`, true)
    }

    setHealth(health) {
        health = Number(health)
        if (isNaN(health)) {
            throw new Error `Health value given is not a number`
        }
        if (health <= 0) {
            health = 0
            this.kill()
            this.emit('died')
        }
        this.health = health
        createLocalIdBuffer(this, 'M')
    }

    setScore(score) {
        score = Number(score)
        if (isNaN(score)) {
            throw new Error `Score value given is not a number`
        }
        this.score = score
        createLocalIdBuffer(this, '0')
        createPacketIds(this, 'X')
    }
    
    setTeam(team) {
        if (!team) return
        this.team = team.teamId
        createLocalIdBuffer(this, 'd')
        createPacketIds(this, 'Y')
    }

    _greet() {
        this._log('\\c8[NOTICE]: This server is proudly hosted with node-hill.')
        this._log(`\\c6[SERVER]: \\c0${this.username} has joined the server!`, true)
    }

    setCameraPosition(pos) {
        this.camXPos = pos[0]
        this.camYPos = pos[1]
        this.camZPos = pos[2]
        createLocalIdBuffer(this, '567')
    }
    
    setCameraRotation(rot) {
        this.camXRot = rot[0]
        this.camYRot = rot[1]
        this.camZRot = rot[2]
        createLocalIdBuffer(this, '89a')
    }

    setCameraFOV(FOV) {
        this.FOV = FOV
        createLocalIdBuffer(this, '3')
    }

    setCameraObject(netId) {
        this.camObj = netId
        createLocalIdBuffer(this, 'c')
    }

    setCameraType(type) {
        if (['fixed', 'orbit', 'free', 'first'].includes(type)) {
            this.camType = type
            createLocalIdBuffer(this, 'b')
        }
    }

    _getClients() {
        let others = new Set(Game.players)

        others.delete(this)

        others.forEach((player) => {
            let packet = new PacketBuilder()

            // Send other players this client
            packet.buffer.writeUInt8(3)

            packet.buffer.writeUInt32LE(this.netId)
            packet.buffer.writeStringNT(this.username)
            packet.buffer.writeUInt32LE(this.userId)
            packet.buffer.writeUInt8(this.admin)

            packet.send(player.socket)

            packet.buffer.clear()

            // Send this client other players
            packet.buffer.writeUInt8(3)

            packet.buffer.writeUInt32LE(player.netId)
            packet.buffer.writeStringNT(player.username)
            packet.buffer.writeUInt32LE(player.userId)
            packet.buffer.writeUInt8(player.admin)

            packet.send(this.socket)
        })
    }

    _updatePositionForOthers(pos) {
        let idBuffer = ""

        if (pos[0] && this.xPos != pos[0]) {
            this.xPos = pos[0]
            idBuffer += "A"
        }

        if (pos[1] && this.yPos != pos[1]) {
            this.yPos = pos[1]
            idBuffer += "B"
        }

        if (pos[2] && this.zPos != pos[2]) {
            this.zPos = pos[2]
            idBuffer += "C"
        }

        if (pos[3] && this.zRot != pos[3]) {
            this.zRot = pos[3]
            idBuffer += "F"
        }

        if (idBuffer.length)
            createMovementIdBuffer(this, idBuffer)
    }

    setPosition(pos) {
        this.xPos = pos[0]
        this.yPos = pos[1]
        this.zPos = pos[2]
        createLocalIdBuffer(this, 'ABCF')
    }

    setScale(scale) {
        this.xScale = scale[0]
        this.yScale = scale[1]
        this.zScale = scale[2]
        createPacketIds(this, 'GHI', true)
    }

    _removeFromPlayerList() {
        const packet = new PacketBuilder()

        packet.buffer.writeUInt8(5)
        packet.buffer.writeUInt32LE(this.netId)

        packet.broadCastExcept(this.socket)
    }

    async setAvatar(userId = null) {
        await scripts.setAvatar(this, userId)
        createLocalIdBuffer(this, 'NOPQRSTUVWXYZ') // Why is it done like this?
        createPacketIds(this, 'KLMNOPQRSTUVW') // Why?
    }

    kill() {
        const packet = new PacketBuilder()

        packet.buffer.writeUInt8(8)
        packet.buffer.writeFloatLE(this.netId)
        packet.buffer.writeUInt8(1)
        
        packet.send(this.socket)

        this.alive = false

        this.health = 0
    }

    respawn() {
        this.setPosition(scripts.pickSpawn())

        let packet = new PacketBuilder()

        packet.buffer.writeUInt8(8)
        packet.buffer.writeFloatLE(this.netId)
        packet.buffer.writeUInt8(0)
        
        packet.send(this.socket)

        this.alive = true

        this.health = this.maxHealth

        this.setCameraType('orbit')

        this.setCameraObject(this.netId)
        
        this.setCameraPosition([this.camXPos, this.camYPos, this.camZPos])
        
        this.setCameraRotation([0, 0, 0])

        this.setCameraFOV(60)

        this.emit('spawned')
    }

    setEnvironment(environment = {}) {
        scripts.setEnvironment(this.socket, environment)
    }

    _getOtherFigures() {
        Game.players.forEach((player) => {
            if (player.loaded && player.netId !== this.netId)
            scripts.createFigure(this.socket, player)
        })
    }

    _getTeams() {
        Game.teams.forEach((team) => {
            scripts.createTeam(this, team)
        })
    }

    setSpeed(speed = 4) {
        speed = Number(speed)
        if (isNaN(speed)) {
            throw new Error `Speed value given is not a number`
        }
        this.maxSpeed = Number(speed)
        createLocalIdBuffer(this, '1')
    }

    setJumpHeight(height = 5) {
        height = Number(height)
        if (isNaN(height)) {
            throw new Error `Jump height value given is not a number`
        }
        this.maxJumpHeight = Number(height)
        createLocalIdBuffer(this, '2')
    }

    async _joined() {
        console.log(`${this.username} has joined | netId: ${this.netId}`)

        this._greet()

        this._getClients()

        this.setEnvironment(Game.world.environment)

        scripts.loadMap(this.socket)

        this._getTeams()

        if (Game.assignRandomTeam)
            this.setTeam(Game.teams[Math.floor(Math.random() * Game.teams.length)])

        if (Game.playersSpawn)
            this.respawn()

        scripts.createFigure(null, this)

        this._getOtherFigures()

        await this.setAvatar().catch(() => {})

        this.loaded = true
    }

}

module.exports = Player
